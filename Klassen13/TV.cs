﻿using System.Windows.Forms;

namespace Klassen13
{
    class TV
    {
        int _volume = 0, _kanaal = 0;

        public TV()
        {
            this.Volume = 0;
            this.Kanaal = 0;
        }

        public int Volume
        {
            get { return _volume; }
            set
            {
                if (value >= 0 && value < 11)
                {
                    _volume = value;
                }
                else
                {
                    MessageBox.Show("Volume is niet correct", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public int Kanaal
        {
            get { return _kanaal; }
            set
            {
                if (value >= 0 && value < 31)
                {
                    _kanaal = value;
                }
                else
                {
                    MessageBox.Show("Kanaal is niet correct", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public void VermeerderKanaal()
        {
            Kanaal++;
        }
        public void VermeerderVolume()
        {
            Volume++;
        }
        public void VerminderKanaal()
        {
            Kanaal--;
        }
        public void VerminderVolume()
        {
            Volume--;
        }
        public string ToonGegevens()
        {
            return "Kanaal: " + Kanaal + " - Volume: " + Volume;
        }
    }
}
