﻿namespace Klassen13
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnKanaalUp = new System.Windows.Forms.Button();
            this.btnKanaalDown = new System.Windows.Forms.Button();
            this.btnVolumeUp = new System.Windows.Forms.Button();
            this.btnVolumeDown = new System.Windows.Forms.Button();
            this.llOutput = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnKanaalUp
            // 
            this.btnKanaalUp.Location = new System.Drawing.Point(12, 45);
            this.btnKanaalUp.Name = "btnKanaalUp";
            this.btnKanaalUp.Size = new System.Drawing.Size(87, 23);
            this.btnKanaalUp.TabIndex = 0;
            this.btnKanaalUp.Text = "Kanaal ++";
            this.btnKanaalUp.UseVisualStyleBackColor = true;
            this.btnKanaalUp.Click += new System.EventHandler(this.btnKanaalUp_Click);
            // 
            // btnKanaalDown
            // 
            this.btnKanaalDown.Location = new System.Drawing.Point(12, 74);
            this.btnKanaalDown.Name = "btnKanaalDown";
            this.btnKanaalDown.Size = new System.Drawing.Size(87, 23);
            this.btnKanaalDown.TabIndex = 1;
            this.btnKanaalDown.Text = "Kanaal --";
            this.btnKanaalDown.UseVisualStyleBackColor = true;
            this.btnKanaalDown.Click += new System.EventHandler(this.btnKanaalDown_Click);
            // 
            // btnVolumeUp
            // 
            this.btnVolumeUp.Location = new System.Drawing.Point(118, 45);
            this.btnVolumeUp.Name = "btnVolumeUp";
            this.btnVolumeUp.Size = new System.Drawing.Size(87, 23);
            this.btnVolumeUp.TabIndex = 2;
            this.btnVolumeUp.Text = "Volume ++";
            this.btnVolumeUp.UseVisualStyleBackColor = true;
            this.btnVolumeUp.Click += new System.EventHandler(this.btnVolumeUp_Click);
            // 
            // btnVolumeDown
            // 
            this.btnVolumeDown.Location = new System.Drawing.Point(118, 74);
            this.btnVolumeDown.Name = "btnVolumeDown";
            this.btnVolumeDown.Size = new System.Drawing.Size(87, 23);
            this.btnVolumeDown.TabIndex = 3;
            this.btnVolumeDown.Text = "Volume --";
            this.btnVolumeDown.UseVisualStyleBackColor = true;
            this.btnVolumeDown.Click += new System.EventHandler(this.btnVolumeDown_Click);
            // 
            // llOutput
            // 
            this.llOutput.AutoSize = true;
            this.llOutput.Location = new System.Drawing.Point(13, 13);
            this.llOutput.Name = "llOutput";
            this.llOutput.Size = new System.Drawing.Size(0, 17);
            this.llOutput.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(216, 108);
            this.Controls.Add(this.llOutput);
            this.Controls.Add(this.btnVolumeDown);
            this.Controls.Add(this.btnVolumeUp);
            this.Controls.Add(this.btnKanaalDown);
            this.Controls.Add(this.btnKanaalUp);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnKanaalUp;
        private System.Windows.Forms.Button btnKanaalDown;
        private System.Windows.Forms.Button btnVolumeUp;
        private System.Windows.Forms.Button btnVolumeDown;
        private System.Windows.Forms.Label llOutput;
    }
}

