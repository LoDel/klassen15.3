﻿using System;
using System.Windows.Forms;

namespace Klassen13
{
    public partial class Form1 : Form
    {
        TV afstandbediening = new TV();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            llOutput.Text = afstandbediening.ToonGegevens();
        }
        private void btnKanaalUp_Click(object sender, EventArgs e)
        {
            afstandbediening.VermeerderKanaal();
            llOutput.Text = afstandbediening.ToonGegevens();
        }

        private void btnKanaalDown_Click(object sender, EventArgs e)
        {
            afstandbediening.VerminderKanaal();
            llOutput.Text = afstandbediening.ToonGegevens();
        }

        private void btnVolumeUp_Click(object sender, EventArgs e)
        {
            afstandbediening.VermeerderVolume();
            llOutput.Text = afstandbediening.ToonGegevens();
        }

        private void btnVolumeDown_Click(object sender, EventArgs e)
        {
            afstandbediening.VerminderVolume();
            llOutput.Text = afstandbediening.ToonGegevens();
        }


    }
}
